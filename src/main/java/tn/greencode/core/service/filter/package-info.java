/**
 * Utilities for JPA criteria classes, used for filtering data on the back-end.
 */
package tn.greencode.core.service.filter;
